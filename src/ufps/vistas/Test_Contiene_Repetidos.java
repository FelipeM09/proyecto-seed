/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author FELIPE MORA
 */
public class Test_Contiene_Repetidos {
    public static void main(String[] args) throws ExceptionUFPS, Exception {
        ListaS<Integer> l=new ListaS();
        l.insertarAlFinal(4234);
        l.insertarAlFinal(1241);
        l.insertarAlFinal(465474);
        l.insertarAlFinal(22233);
        l.insertarAlFinal(247452);
        l.insertarAlFinal(13322);
        l.insertarAlFinal(235);
        l.insertarAlFinal(657);
        l.insertarAlFinal(3552);
        l.insertarAlFinal(5655);
        l.insertarAlFinal(3552);
        l.insertarAlFinal(7453);
        l.insertarAlFinal(5223355);
        l.insertarAlFinal(132);
        l.insertarAlFinal(8234);
        l.insertarAlFinal(4666); 
        
        
                
        System.out.println("Lista Original:\n"+l.toString()); 
        System.out.println("Contiene Elementos Repetidos:" + "\n" + l.contieneElemRepetidos());
        
        
        
    }
    
}

